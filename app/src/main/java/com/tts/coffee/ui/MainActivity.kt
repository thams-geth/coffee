package com.tts.coffee.ui

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.tts.coffee.R
import com.tts.coffee.ui.addprofile.AddProfileActivity
import com.tts.coffee.ui.addprofile.User
import com.tts.coffee.ui.admin.AdminActivity
import com.tts.coffee.ui.cart.CartActivity
import com.tts.coffee.ui.login.LoginActivity
import kotlinx.android.synthetic.main.content_main.*
import java.util.*


class MainActivity : AppCompatActivity() {
    private val PREF_NAME = "coffee"
    lateinit var sharedPref: SharedPreferences

    private lateinit var auth: FirebaseAuth
    private val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        setSupportActionBar(toolbar)


        sharedPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        auth = FirebaseAuth.getInstance()

        updateDetails()

        if (auth.currentUser != null) {
            val docRef = db.collection("users").document(auth.currentUser!!.email!!)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document.data != null) {
                        Log.d(TAG, "DocumentSnapshot data: ${document.data}")
                        val user: User = document.toObject(User::class.java)!!
                        val editor = sharedPref.edit()
                        editor.putBoolean("paperCup", user.paperCup)
                        editor.putString("firstName", user.firstName)
                        editor.putString("lastName", user.lastName)
                        editor.apply()

                    } else {
                        Log.d(TAG, "No such document")
//
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(TAG, "get failed with ", exception)
                }
        }


//        val user = hashMapOf(
//            "first" to "Ada",
//            "last" to "Lovelace",
//            "born" to 1815
//        )
//
//        // Add a new document with a generated ID
//        db.collection("users")
//            .add(user)
//            .addOnSuccessListener { documentReference ->
//                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
//            }
//            .addOnFailureListener { e ->
//                Log.w(TAG, "Error adding document", e)
//            }
//
//        db.collection("users")
//            .get()
//            .addOnSuccessListener { result ->
//                for (document in result) {
//                    Log.d(TAG, "${document.id} => ${document.data}")
//                }
//            }
//            .addOnFailureListener { exception ->
//                Log.w(TAG, "Error getting documents.", exception)
//            }
//
//
//        val city = hashMapOf(
//            "name" to "Los Angeles",
//            "state" to "CA",
//            "country" to "USA"
//        )
//
//        val updates = hashMapOf<String, Any>(
//            "timestamp" to FieldValue.serverTimestamp()
//        )
//        db.collection("cities").document("LA")
//            .set(city)
//            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
//            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

//        val user = HashMap()
//        user.put("first", "Ada")
//        user.put("last", "Lovelace")
//        user.put("born", 1815)


        ivUser.setOnClickListener {
            if (auth.currentUser != null) {
                if (sharedPref.getBoolean("login", false) && sharedPref.getString(
                        "firstName",
                        ""
                    ) != ""
                ) {
                    Log.d(TAG, "in if case")
                    val user: User = User(
                        sharedPref.getString("firstName", "")!!,
                        sharedPref.getString("lastName", "")!!,
                        sharedPref.getBoolean("paperCup", true)
                    )
                    startActivity(
                        Intent(this, AddProfileActivity::class.java)
                            .putExtra("email", auth.currentUser!!.email)
                            .putExtra("newUser", false)
                            .putExtra("user", user)
                    )

                } else {
                    Log.d(TAG, "in else case")

                    startActivity(
                        Intent(this, AddProfileActivity::class.java)
                            .putExtra("email", auth.currentUser!!.email).putExtra("newUser", true)
                    )
                }
            } else {
                Toast.makeText(this, "Please Login", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }

        val imageView = findViewById<ImageView>(R.id.imageView)

        val ivCoffee = findViewById<ImageView>(R.id.ivCoffee)
        val ivTea = findViewById<ImageView>(R.id.ivTea)
        val ivGreentea = findViewById<ImageView>(R.id.ivGreentea)
        val ivMilk = findViewById<ImageView>(R.id.ivMilk)

        Glide.with(this).load(R.raw.cofeegif).fitCenter().into(imageView)
        Glide.with(this).load(R.raw.coffee_op).fitCenter().into(ivCoffee)
        Glide.with(this).load(R.raw.tea_op).fitCenter().into(ivTea)
        Glide.with(this).load(R.raw.green_tea_op).fitCenter().into(ivGreentea)
        Glide.with(this).load(R.raw.milk_op).fitCenter().into(ivMilk)

        cardCoffee.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            startActivity(intent)
        }

        cardTea.setOnClickListener {
            val intent = Intent(this, AdminActivity::class.java)
            startActivity(intent)
        }


//        fab.setOnClickListener { view ->
//            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show()
//        }
    }

    private fun updateDetails() {
        if (sharedPref.getBoolean("login", false)) {
            val name = sharedPref.getString("firstName", "User").toString()
            tvName.text = "Hello " + name.substring(0, 1).toUpperCase() + name.substring(1)
        }

        val c: Calendar = Calendar.getInstance()
        val timeOfDay: Int = c.get(Calendar.HOUR_OF_DAY)

        if (timeOfDay >= 0 && timeOfDay < 12) {
            tvGreetings.text = "Good Morning"
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            tvGreetings.text = "Good Afternoon"
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            tvGreetings.text = "Good Evening"
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            tvGreetings.text = "Good Night"
        }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(user: FirebaseUser?) {
//        if (user != null) {
//            tvEmail.text = user.email
//        } else {
//            tvEmail.text = "Have a Great Day"
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                auth.signOut()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}
