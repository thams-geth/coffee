package com.tts.coffee.ui.admin

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.tts.coffee.R
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity(), InterfaceConfirmClick {

    private var firestore = FirebaseFirestore.getInstance()
    private var orderList = ArrayList<OrderModel>()
    private lateinit var orderListAdapter: OrderListAdapter
    private lateinit var query: Query


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)


        query = firestore.collection("orders").orderBy("timestamp", Query.Direction.DESCENDING)
        orderListAdapter = OrderListAdapter(orderList, this)
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = orderListAdapter
        getData()


    }

    private fun getData() {
        orderList.clear()

        query.get().addOnSuccessListener {
            if (it.documents.size > 0) {
                for (doc in it.documents) {
                    var order = doc.toObject(OrderModel::class.java)!!
                    order.docID = doc.id
                    Log.d(TAG, order.toString() + " Doc ID : " + doc.id)
                    orderList.add(order)
                }

                orderListAdapter.notifyDataSetChanged()
            }
        }

    }

    companion object {
        const val TAG = "AdminActivity"
    }

    override fun onConfirmClick(docID: String) {
        firestore.collection("orders").document(docID).update("status", "accepted").addOnSuccessListener {
            getData()
        }
    }

    override fun onCancelClick(docID: String) {
        firestore.collection("orders").document(docID).update("status", "canceled").addOnSuccessListener {
            getData()
        }
    }
}
