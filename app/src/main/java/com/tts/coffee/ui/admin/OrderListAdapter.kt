package com.tts.coffee.ui.admin

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.coffee.R
import kotlinx.android.synthetic.main.item_order_list.view.*
import java.util.*

class OrderListAdapter(var orderModel: ArrayList<OrderModel>, var listener: InterfaceConfirmClick) :
    RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {

    var inteface: InterfaceConfirmClick = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_order_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return orderModel.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.tvName.text = orderModel[position].name!!.substring(0, 1).toUpperCase() + orderModel[position].name!!.substring(1)

        if (orderModel[position].status == "ordered") {
            holder.itemView.btnCancel.visibility = View.VISIBLE
            holder.itemView.btnConfirm.setBackgroundResource(R.drawable.btn_back)
            holder.itemView.btnConfirm.isEnabled = true
        } else if (orderModel[position].status == "accepted") {
            holder.itemView.btnCancel.visibility = View.INVISIBLE
            holder.itemView.btnConfirm.text = "Accepted"
            holder.itemView.btnConfirm.setBackgroundResource(R.drawable.btn_back_green)
            holder.itemView.btnConfirm.isEnabled = false
        } else if (orderModel[position].status == "canceled") {
            holder.itemView.btnCancel.visibility = View.VISIBLE
            holder.itemView.btnCancel.isEnabled = false
            holder.itemView.btnCancel.text = "C a n c e l e d"
            holder.itemView.btnConfirm.visibility = View.GONE
        }
        holder.itemView.btnConfirm.setOnClickListener {
            inteface.onConfirmClick(orderModel[position].docID!!)
        }
        holder.itemView.btnCancel.setOnClickListener {
            inteface.onCancelClick(orderModel[position].docID!!)
        }
        holder.itemView.tvItems.text = orderItemsDetails(orderModel[position].order)
    }

    private fun orderItemsDetails(order: HashMap<String, Items>?): CharSequence? {

        var s = ""
        if (order != null) {
            for (i in 0 until order.size) {
                s = s + order["" + i]!!.quantity + " x " + order["" + i]!!.name + " \n"
            }
        }

        return s
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}