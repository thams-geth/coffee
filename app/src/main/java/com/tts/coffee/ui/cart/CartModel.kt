package com.tts.coffee.ui.cart

data class CartModel(var name: String, var quantity: Int)