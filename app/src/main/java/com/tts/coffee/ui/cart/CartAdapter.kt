package com.tts.coffee.ui.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.coffee.R
import kotlinx.android.synthetic.main.item_cart.view.*

class CartAdapter(var name: ArrayList<String>, var onClick: InterfaceOnItemClick) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {


    var orderList = ArrayList<CartModel>()
    var inteface: InterfaceOnItemClick = onClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_cart, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return name.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvName.text = name[position]

        holder.itemView.tvAdd.setOnClickListener {
            holder.itemView.tvAdd.visibility = View.GONE
            holder.itemView.llAddandMinus.visibility = View.VISIBLE

            //setting item count
            holder.itemView.tvQuantity.text = "" + (holder.itemView.tvQuantity.text.toString().toInt() + 1)
            orderList.add(
                CartModel(
                    holder.itemView.tvName.text.toString(),
                    holder.itemView.tvQuantity.text.toString().toInt()
                )
            )
            inteface.onItemClick(orderList)

        }

        holder.itemView.ivMinus.setOnClickListener {

            val orderListItemPosition =
                orderList.lastIndexOf(
                    CartModel(
                        holder.itemView.tvName.text.toString(),
                        holder.itemView.tvQuantity.text.toString().toInt()
                    )
                )
            holder.itemView.tvQuantity.text = "" + (holder.itemView.tvQuantity.text.toString().toInt() - 1)


            if (holder.itemView.tvQuantity.text.toString().toInt() == 0) {
                holder.itemView.tvAdd.visibility = View.VISIBLE
                holder.itemView.llAddandMinus.visibility = View.GONE
                orderList.removeAt(orderListItemPosition)
            } else {
                orderList[orderListItemPosition] =
                    CartModel(holder.itemView.tvName.text.toString(), holder.itemView.tvQuantity.text.toString().toInt())


            }
            inteface.onItemClick(orderList)


        }
        holder.itemView.ivPlus.setOnClickListener {

            val orderListItemPosition =
                orderList.lastIndexOf(
                    CartModel(
                        holder.itemView.tvName.text.toString(),
                        holder.itemView.tvQuantity.text.toString().toInt()
                    )
                )

            if (holder.itemView.tvQuantity.text.toString().toInt() != 10) {
                holder.itemView.tvQuantity.text = "" + (holder.itemView.tvQuantity.text.toString().toInt() + 1)
                orderList[orderListItemPosition] =
                    CartModel(holder.itemView.tvName.text.toString(), holder.itemView.tvQuantity.text.toString().toInt())

            }

            inteface.onItemClick(orderList)

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}