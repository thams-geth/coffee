package com.tts.coffee.ui.addprofile

import java.io.Serializable


data class User(
    var firstName: String = "",
    var lastName: String = "",
    var paperCup: Boolean = true
) : Serializable