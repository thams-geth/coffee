package com.tts.coffee.ui.addprofile

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.tts.coffee.R
import kotlinx.android.synthetic.main.activity_add_profile.*

class AddProfileActivity : AppCompatActivity() {

    private val PREF_NAME = "coffee"
    lateinit var sharedPref: SharedPreferences

    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_profile)

        sharedPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        val email: String = intent.getStringExtra("email")!!
        val newUser: Boolean = intent.getBooleanExtra("newUser", true)
        Log.d(TAG, email + newUser)

        if (newUser) {
            Log.d(TAG, "in if case")

            cbPaperCup.isChecked = true
            cbMyCup.isChecked = false

        } else {

            val user: User = intent.getSerializableExtra("user") as User
            Log.d(TAG, "in else case " + user.toString())

            etFirstName.setText(user.firstName)
            etLastName.setText(user.lastName)
            if (user.paperCup) {
                cbPaperCup.isChecked = true
                cbMyCup.isChecked = false
            } else {
                cbPaperCup.isChecked = false
                cbMyCup.isChecked = true
            }
        }


        etEmail.setText(email)
        cbMyCup.setOnCheckedChangeListener { buttonView, isChecked ->
            cbPaperCup.isChecked = !isChecked
        }
        cbPaperCup.setOnCheckedChangeListener { buttonView, isChecked ->
            cbMyCup.isChecked = !isChecked
        }

        btnSave.setOnClickListener {

            if (isValid()) {
                showProgressBar()
                val user = User(
                    etFirstName.text.toString(),
                    etLastName.text.toString(),
                    cbPaperCup.isSelected
                )

                db.collection("users").document(email).set(user).addOnSuccessListener {
                    Log.d(TAG, "Date Stored Successfully")
                    val editor = sharedPref.edit()
                    editor.putBoolean("paperCup", cbPaperCup.isSelected)
                    editor.putString("firstName", etFirstName.text.toString())
                    editor.putString("lastName", etLastName.text.toString())
                    editor.putString("email", etEmail.text.toString())
                    editor.apply()
                    finish()
                    hideProgressBar()
                }
            }
        }

    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
        btnSave.isEnabled = false
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
        btnSave.isEnabled = true

    }

    private fun isValid(): Boolean {

        if (etFirstName.text.toString() == "") {
            etFirstName.error = "Please Enter FirstName"
            return false
        }
        if (etLastName.text.toString() == "") {
            etLastName.error = "Please Enter Last Name"
            return false
        }

        return true

    }

    companion object {
        private const val TAG = "AddProfileActivity"
    }

}
