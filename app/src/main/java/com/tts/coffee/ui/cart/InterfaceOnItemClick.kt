package com.tts.coffee.ui.cart

interface InterfaceOnItemClick {

    fun onItemClick(orderList: ArrayList<CartModel>)
}