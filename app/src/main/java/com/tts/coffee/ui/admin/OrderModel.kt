package com.tts.coffee.ui.admin

import java.util.*

class OrderModel {
    val name: String? = null
    val email: String? = null
    val status: String? = null
    val token: String? = null
    val timestamp: Date? = null
    var docID: String? = null
    val order: HashMap<String, Items>? = null
}