package com.tts.coffee.ui.cart

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.tts.coffee.R
import kotlinx.android.synthetic.main.activity_cart.*

class CartActivity : AppCompatActivity(), InterfaceOnItemClick {

    private var db = FirebaseFirestore.getInstance()
    private var order = ArrayList<CartModel>()


    private val PREF_NAME = "coffee"
    lateinit var sharedPref: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        sharedPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        val name = arrayListOf<String>(
            "Strong Coffee",
            "Lite Coffee",
            "Strong Tea",
            "Lite Tea",
            "Milk",
            "Green Tea",
            "Black Tea",
            "Black Coffee"
        )
        cardView.visibility = View.GONE
        progressBar.visibility = View.INVISIBLE
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = CartAdapter(name, this)

        btnOrder.setOnClickListener {

            showProgressDialog()

            val mapOrderItems = HashMap<String, Any>()
            for (x in 0 until order.size)
                mapOrderItems["" + x] = order[x]
            val map = HashMap<String, Any>()
            map["order"] = mapOrderItems
            map["name"] = sharedPref.getString("firstName", "defName").toString() + " " + sharedPref.getString("lastName", "Thams").toString()
            map["email"] = sharedPref.getString("email", "defMail").toString()
            map["status"] = "ordered"
            map["token"] = sharedPref.getString("token", "").toString()
            map["timestamp"] = FieldValue.serverTimestamp()

            db.collection("orders").add(map).addOnSuccessListener {
                Log.d(TAG, "Order Placed Successfully")
                Toast.makeText(this, "Order Placed Successfully", Toast.LENGTH_SHORT).show()
                hideProgressDialog()
                finish()
            }
        }
    }


    private fun showProgressDialog() {
        progressBar.visibility = View.VISIBLE
        btnOrder.isEnabled = false
    }

    private fun hideProgressDialog() {
        progressBar.visibility = View.INVISIBLE
        btnOrder.isEnabled = true

    }


    override fun onItemClick(orderList: ArrayList<CartModel>) {
        Log.d(TAG, orderList.toString())
        order = orderList
        if (orderList.size <= 0) {
            cardView.visibility = View.GONE
        } else {
            cardView.visibility = View.VISIBLE
            var quantity = 0
            for (i in orderList) {
                quantity += i.quantity
            }
            tvOrderItems.text = "$quantity Items"
        }


    }

    companion object {
        private const val TAG = "CartActivity"
    }
}
