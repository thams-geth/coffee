package com.tts.coffee.ui.admin

interface InterfaceConfirmClick {

    fun onConfirmClick(docID: String)
    fun onCancelClick(docID: String)
}