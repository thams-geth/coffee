package com.tts.coffee.ui.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.iid.FirebaseInstanceId
import com.tts.coffee.R
import com.tts.coffee.ui.admin.AdminActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val PREF_NAME = "coffee"
    lateinit var sharedPref: SharedPreferences

    private lateinit var auth: FirebaseAuth

    private var db = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        auth = FirebaseAuth.getInstance()
        sharedPref = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)


        val imageView = findViewById<ImageView>(R.id.imageView)
        Glide.with(this).load(R.raw.cofeegif).fitCenter().into(imageView)

        btnSignin.setOnClickListener {
            Log.d(TAG, "SignIn clicked ")

            if (isValid()) {

                showProgressDialog()

                // [START sign_in_with_email]
                auth.signInWithEmailAndPassword(
                    etUserName.text.toString(),
                    etPassword.text.toString()
                )
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success")
                            val user = auth.currentUser
                            updateUI(user)
                        } else {
                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "signInWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed." + task.exception!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            updateUI(null)
                        }
                        hideProgressDialog()
                    }
                // [END sign_in_with_email]
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(user: FirebaseUser?) {

        hideProgressDialog()

        if (user != null) {

            Log.d(
                TAG, user.email +
                        user.isEmailVerified +
                        user.uid
            )
            if (user.email == "admin@gmail.com") {
                val intent = Intent(this, AdminActivity::class.java)
                startActivity(intent)
            } else {
                val editor = sharedPref.edit()
                FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Log.w(TAG, "getInstanceId failed", task.exception)
                            return@OnCompleteListener
                        }

                        // Get new Instance ID token
                        val token = task.result!!.token
                        val tokenMap = hashMapOf(
                            "token" to token
                        )
                        db.collection("users").document(user.email!!).collection("token")
                            .document("token").set(tokenMap)


                        // Log and toast
                        Log.d(TAG, token)
                        Toast.makeText(baseContext, token, Toast.LENGTH_SHORT).show()

                        editor.putBoolean("login", true)
                        editor.putString("token", token)
                        editor.putString("email", user.email)
                        editor.apply()
                        finish()
                    })


            }


//            emailPasswordButtons.visibility = View.GONE
//            emailPasswordFields.visibility = View.GONE
//            signedInButtons.visibility = View.VISIBLE
//            verifyEmailButton.isEnabled = !user.isEmailVerified
        } else {

//            emailPasswordButtons.visibility = View.VISIBLE
//            emailPasswordFields.visibility = View.VISIBLE
//            signedInButtons.visibility = View.GONE
        }
    }

    private fun showProgressDialog() {
        progressBar.visibility = View.VISIBLE
        btnSignin.isEnabled = false
    }

    private fun hideProgressDialog() {
        progressBar.visibility = View.INVISIBLE
        btnSignin.isEnabled = true

    }

    private fun isValid(): Boolean {

        if (etUserName.text.toString() == "") {
            etUserName.error = "Please Enter Username"
            return false
        }
        if (etPassword.text.toString() == "") {
            etPassword.error = "Please Enter Valid Password"
            return false
        } else if (etPassword.text.length < 6) {
            etPassword.error = "Password should be 6 or more"
            return false
        }
        return true

    }

    companion object {
        private const val TAG = "LoginActivity"
    }
}
